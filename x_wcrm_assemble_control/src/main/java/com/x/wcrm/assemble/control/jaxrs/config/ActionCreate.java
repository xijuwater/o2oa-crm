package com.x.wcrm.assemble.control.jaxrs.config;

import com.google.gson.JsonElement;
import com.x.base.core.container.EntityManagerContainer;
import com.x.base.core.container.factory.EntityManagerContainerFactory;
import com.x.base.core.entity.JpaObject;
import com.x.base.core.entity.annotation.CheckPersistType;
import com.x.base.core.project.bean.WrapCopier;
import com.x.base.core.project.bean.WrapCopierFactory;
import com.x.base.core.project.http.ActionResult;
import com.x.base.core.project.http.EffectivePerson;
import com.x.base.core.project.jaxrs.WoId;
import com.x.base.core.project.logger.Logger;
import com.x.base.core.project.logger.LoggerFactory;
import com.x.wcrm.assemble.control.jaxrs.record.BaseAction;
import com.x.wcrm.core.entity.Leads;
import com.x.wcrm.core.entity.WCrmConfig;
import org.apache.commons.lang3.StringUtils;

public class ActionCreate extends BaseAction {

	private static Logger logger = LoggerFactory.getLogger(ActionCreate.class);

	ActionResult<Wo> execute(EffectivePerson effectivePerson, JsonElement jsonElement) throws Exception {
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			ActionResult<Wo> result = new ActionResult<>();
			Wi wi = this.convertToWrapIn(jsonElement, Wi.class);
			WCrmConfig o = Wi.copier.copy(wi);
			emc.beginTransaction(WCrmConfig.class);
			emc.persist(o, CheckPersistType.all);
			emc.commit();
			
			Wo wo = new Wo();
			wo.setId(o.getId());
			result.setData(wo);
			return result;
		}
	}

	static class Wi extends WCrmConfig {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7106839105196570589L;
		static WrapCopier<Wi, WCrmConfig> copier = WrapCopierFactory.wi(Wi.class, WCrmConfig.class, null, JpaObject.FieldsUnmodify);

	}

	static class Wo extends WoId {
		static WrapCopier<WCrmConfig, Wo> copier = WrapCopierFactory.wo(WCrmConfig.class, Wo.class, null, JpaObject.FieldsInvisible);
	}

}
